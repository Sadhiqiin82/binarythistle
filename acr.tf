data "azurerm_container_registry" "mosa-acr" {
  name = "mosaacr"
  resource_group_name = "mosa-rg"
}

resource "azurerm_role_assignment" "aks-to-acr" {
  scope                = data.azurerm_container_registry.mosa-acr.id
  role_definition_name = "AcrPull"
  principal_id         = data.azuread_service_principal.terraform-exec.object_id
  skip_service_principal_aad_check = true  
}
