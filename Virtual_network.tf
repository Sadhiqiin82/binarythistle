resource "azurerm_virtual_network" "mosa-vnet" {
  name                = "${lower(var.custcode)}-aks--prd-vnet"
  resource_group_name = azurerm_resource_group.mosa-aks-rg.name
  address_space       = [var.addreess_prefix]
  location            = azurerm_resource_group.mosa-aks-rg.location
}

resource "azurerm_subnet" "mosa-subnet" {
  name                 = "${lower(var.custcode)}-aks-prd-subnet"
  resource_group_name  = azurerm_resource_group.mosa-aks-rg.name
  virtual_network_name = azurerm_virtual_network.mosa-vnet.name
  address_prefixes     = [var.subnet_prefix]
}

