resource "azurerm_resource_group" "mosa-aks-rg" {
  name     = "${lower(var.custcode)}-aks-rg"
  location = var.location

  tags = {
    environment = var.environment
    location = var.location
    custcode = var.custcode
  }
}

data "azuread_service_principal" "terraform-exec"{
    display_name = "terraform-exec"
}