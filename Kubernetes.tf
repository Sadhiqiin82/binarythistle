resource "azurerm_kubernetes_cluster" "mosa-aks" {
  name                = var.aks_name
  resource_group_name = azurerm_resource_group.mosa-aks-rg.name
  location            = azurerm_resource_group.mosa-aks-rg.location
  dns_prefix          = "mosa-aks"

  default_node_pool {
    name             = var.nodepool_name
    node_count       = var.initial_node_count
    vm_size          = var.node_vm_size
    os_disk_size_gb  = var.node_disk_size
    vnet_subnet_id   = azurerm_subnet.mosa-subnet.id
    max_pods         = var.max_pods

    enable_auto_scaling   = true
    min_count        = var.min_node_count
    max_count        = var.max_node_count
  }

network_profile {
    network_plugin     = "azure"
    network_policy     = "azure"
    service_cidr       = var.service_cidr
    dns_service_ip     = "10.0.0.10"
    docker_bridge_cidr = "172.17.0.1/16"
  }

  service_principal {
    client_id     = data.azuread_service_principal.terraform-exec.application_id
    client_secret = var.client_secret
  }
}

resource "azurerm_log_analytics_workspace" "mosa-aks-work-space" {
  name                = "mosa-aks-prd-ws"
  resource_group_name = azurerm_resource_group.mosa-aks-rg.name
  location            = azurerm_resource_group.mosa-aks-rg.location
  sku                 = "PerGB2018"
  retention_in_days   = 30
}

  resource  "azurerm_monitor_diagnostic_setting" "mosa-aks-ws" {
  name                       = "${azurerm_kubernetes_cluster.mosa-aks.name}-audit"
  target_resource_id         = azurerm_kubernetes_cluster.mosa-aks.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.mosa-aks-work-space.id

  log {
    category = "kube-apiserver"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-controller-manager"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "cluster-autoscaler"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-scheduler"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-audit"
    enabled  = true

    retention_policy {
      enabled = false
    }
  }

  metric {
    category = "AllMetrics"
    enabled  = false

    retention_policy {
      enabled = false
    }
  }
}

    

