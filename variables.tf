#main#
variable "custcode" {
}
variable "location" {
}
variable "environment" {
}

variable "client_id" {
}
variable "client_secret" {
}

#Kubernetes#
variable "addreess_prefix" {
}
variable "subnet_prefix" {
}
variable "aks_name" {
}
variable "nodepool_name" {
}
variable "initial_node_count" {
}
variable "node_vm_size" {
}
variable "node_disk_size" {
}
variable "max_node_count" {
}
variable "min_node_count" {
}
variable "max_pods" {
}
variable "service_cidr" {
}













